#!/bin/bash

# Clone cerbero
git clone --depth=1 git://anongit.freedesktop.org/gstreamer/cerbero

# Apply any patches
echo "Checking for any patches..."
if [ -d patches ] && [ -n "$(ls -A patches)" ]; then
    for patch in patches/*.patch
    do
        git -C cerbero/ am --signoff < "$patch"
    done
else
    echo "None found..."
fi

# Move config file into cerbero
mv cross-lin-rpi.cbc cerbero/config

# Extract sysroot into cerbero
echo -n "Extracting sysroot to cerbero/config/raspbian-sysroot..."
mkdir cerbero/config/raspbian-sysroot && tar --exclude="./dev" --exclude="./run" -xzf raspbian-sysroot.tar.gz -C cerbero/config/raspbian-sysroot
echo "Done."

echo
echo "Setup done"
