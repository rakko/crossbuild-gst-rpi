Build environment image for cross-building GStreamer for the Raspberry Pi.

---

This image bundles the packages and tools necessary for cross-building and packaging GStreamer for the Raspberry Pi using cerbero.

Specifically, the environment targets the 32-bit hard-float ARM architecture (armhf) running Raspbian.

The repository does not come with a Raspbian sysroot but you can build one using [KVM](https://www.linux-kvm.org/page/Main_Page), [debos](https://github.com/go-debos/debos), and [Docker](https://www.docker.com/get-docker) as such:

`docker run --rm --device /dev/kvm -u $(id -u) -v $(pwd):/build docker-registry.collabora.com/debos /bin/bash -c "cd /build; debos raspbian-sysroot.yaml"`

This will build a raspbian sysroot based on the `YAML` file in the current directory. You can modify the `YAML` file to suit your needs. Similarly, you can modify the `cross-lin-rpi.cbc` cerbero config file to match your achitecture.

To use image you can build it by:

`docker build -t crossbuild-gst-rpi .`

but you can also use an unmodified built image from dockerhub by doing `docker pull rakko/crossbuild-gst-rpi`.

Now, we just have to make an output directory to place our final package, run our image, and execute the following commands in it:

```
$ mkdir output
$ docker run --rm -it -v $(pwd)/output:/gstbuild/output rakko/crossbuild-gst-rpi /bin/bash
gstbuild@...$ ./setup.sh
gstbuild@...$ cerbero/cerbero-uninstalled -c cerbero/config/cross-lin-rpi.cbc bootstrap --build-tools-only
gstbuild@...$ cerbero/cerbero-uninstalled -c cerbero/config/cross-lin-rpi.cbc package -t gstreamer-1.0 -o output/
```

Obviously this can all be scripted but many things can go wrong, especially if anything is modified, so try running the steps one by one on your first few runs.
