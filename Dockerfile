FROM debian:stretch

# Install packages for cross toolchains and cerbero bootstrap
RUN dpkg --add-architecture armhf && \
    apt-get update && \
    apt-get -y install \
        autoconf automake autopoint autotools-dev bison build-essential ccache chrpath \
        cmake crossbuild-essential-armhf curl debhelper devscripts doxygen dpkg-dev fakeroot \
        flex g++ gettext git glib-networking gperf gtk-doc-tools intltool libasound2-dev \
        libdbus-glib-1-dev libfuse-dev libpulse-dev libtool libxml-simple-perl make \
        pkg-config python3-dev python3-setuptools subversion sudo texinfo transfig wget yasm \
        # X11
        libx11-dev libxcomposite-dev libxdamage-dev libxext-dev libxfixes-dev libxi-dev \
        libxrandr-dev libxrender-dev libxtst-dev libxv-dev x11proto-record-dev xutils-dev \
        # OpenGL
        libegl1-mesa-dev libgl1-mesa-dev libglu1-mesa-dev

# Add user UID:1000, GID:1000, home at /gstbuild
RUN groupadd -r gstbuild -g 1000 && \
    useradd -u 1000 -r -g gstbuild -G sudo -m -d /gstbuild -s /sbin/nologin -c "Gstbuild user" gstbuild

# Add user gstbuild to sudoers without requiring password to run commands
RUN echo "gstbuild:gstbuild" | chpasswd
RUN echo "gstbuild ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

# Set the working directory to gstbuild home directory
WORKDIR /gstbuild

# Copy project
COPY . .

# Permissions for all users and groups
RUN mkdir -p /opt/gstreamer-1.0 && chmod -R 777 /gstbuild /opt/gstreamer-1.0

# Change user
USER gstbuild

# Configure git and cerbero
# TODO: I should not be using my own email, find something else to use
RUN git config --global user.email "info@collabora.com" && \
    git config --global user.name "Collabora Git"

